import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

Vue.config.productionTip = false

Vue.use(VueMoment, {
  moment,
})
Vue.filter('moment', function (value, format) {
  if (value === null || value === undefined || format === undefined) {
    return ''
  }
  if (format === 'from') {
    return moment(value).fromNow()
  }
  return moment(value).format(format)
})
new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
